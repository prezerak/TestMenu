import java.util.ArrayList;
import java.util.Scanner;

public class Menu {
	
	private ArrayList<Option> optionsList = new ArrayList<Option>();
	private byte selection;


	public void addOption(byte i, String s) {
		
		if (!optionsList.stream().anyMatch(o -> o.getIndex() == i || o.getLabel().equalsIgnoreCase(s)))
		{		
			optionsList.add(new Option(i, s));
		}
	}
	
	public void printAll()
	{
		Option[] sortedList = 
				optionsList.stream().sorted((o1, o2)-> Integer.compare(o2.getIndex(), o1.getIndex()))
				.toArray(size -> new Option[size]);
				 
		
		for(Option o : sortedList) {					
			System.out.println(o.getIndex()+". "+o.getLabel());
		}
		
		System.out.println();
		System.out.print("Please choose:");
	}

	public void readSelection() {
		Scanner scanner = new Scanner(System.in);
		do
		{		
			while (!scanner.hasNextByte()) {
			scanner.next();
			}
			selection = scanner.nextByte();
		} while (!optionsList
					.stream()
					.anyMatch(x -> x.getIndex() == selection));
					
		System.out.println("The user has selected: " + selection);
		scanner.close();
	}
}
