
public class Option {
	private byte index;
	private String label;
	
	public Option(byte index, String label)
	{
		this.index = index;
		this.label = label;
	}

	public byte getIndex() {
		return index;
	}

	
	public String getLabel() {
		return label;
	}

	
}
